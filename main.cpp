#include <iostream>
#include <memory>
#include "pilang.hpp"


int main() {
    using namespace Pilang3;

    // Initialise
    std::string line;
    auto env = std::make_shared<Environment>();

    // CLI loop
    while (true) {
        std::cout << "> " << std::flush;

        std::string thisline;
        std::getline(std::cin, thisline);
        line.append(thisline);

        try {
            Evaluation evaluation(env, line);
            evaluation.derefer(env);
            evaluation.execute(env);
            line.clear();
        } catch (exceptions::emptyExpr&) {
            // Do nothing
        } catch (exceptions::UnexpectedEndOfExpression&) {
            std::cout << "    ";
        } catch (exceptions::exit& e) {
            return e.code;
        } catch (exceptions::langException& e) {
            std::cout << "E: Language exception: " << e.what() << std::endl;
            line.clear();
        }
    }
}
