#include <iostream>
#include "pilang.hpp"
using namespace Pilang3;


class StdIO {
public:
    static Variable print(SharedEnvironment, Cmdargs& args) {
        std::cout << std::get<std::string>(args[0].data) << std::endl;
        return Variable({
            Variable::id_null,
            0
        });
    }

    static Variable input(SharedEnvironment, Cmdargs&) {
        std::string fres;
        std::getline(std::cin, fres);
        return Variable({
            Variable::id_string,
            fres
        });
    }

    StdIO() {
        builtinCmds["print"] = {print, {Variable::id_string}, false};
        builtinCmds["input"] = {input, {}, false};
    }
};

static StdIO inst;
